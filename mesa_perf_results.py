import datetime
import flask_compress
import git
import math
import os
import time
import urllib
from flask import (Flask, make_response, redirect, render_template,
                   request, url_for)
from flask_caching import Cache
from scipy import stats
from pony.orm import db_session
from pony import orm

from models import db


class ReverseProxied():
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        script_name = environ.get("HTTP_X_SCRIPT_NAME", "")
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]
            scheme = environ.get("HTTP_X_SCHEME", "")
            if scheme:
                environ["wsgi.url_scheme"] = scheme
        return self.app(environ, start_response)


app = Flask(__name__)
flask_compress.Compress(app)
app.wsgi_app = ReverseProxied(app.wsgi_app)
cache = Cache(app, config={'CACHE_TYPE': 'simple'})

secure_cookie = True
# Allow disabling the secure cookie flag (e.g. when running for development)
if 'SESSION_COOKIE_SECURE' in os.environ:
    secure_cookie = os.environ.get('SESSION_COOKIE_SECURE').lower()
    secure_cookie = secure_cookie == 'true' or secure_cookie == '1'
# Flag for indicating whether site is internal or not. Used to hide or
# display certain elements of the site that don't make sense if site is
# not internal
internal_site = False
if 'SITE_IS_INTERNAL' in os.environ:
    internal_site = os.environ.get('SITE_IS_INTERNAL').lower()
    internal_site = internal_site == 'true' or internal_site == '1'

# Initialize db object
sql_user = 'jenkins'
if "SQL_DATABASE_USER" in os.environ:
    sql_user = os.environ["SQL_DATABASE_USER"]

sql_pw = ''
if "SQL_DATABASE_PW_FILE" in os.environ:
    sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
    if os.path.exists(sql_pw_file):
        with open(sql_pw_file, 'r') as f:
            sql_pw = f.read().rstrip()
sql_host = "localhost"
if "SQL_DATABASE_HOST" in os.environ:
    sql_host = os.environ["SQL_DATABASE_HOST"]

print("************************** " + sql_pw)
connected = False
while not connected:
    try:
        if sql_pw:
            db.bind('mysql', host=sql_host, user=sql_user, password=sql_pw,
                    database='mesa_perf_results')
        else:
            db.bind('mysql', host=sql_host, user=sql_user,
                    database='mesa_perf_results')
    except orm.dbapiprovider.OperationalError:
        print('Unable to connect to SQL database, retrying in 5 seconds...')
        time.sleep(5)
        continue
    print("Connected to SQL database!")
    connected = True
db.generate_mapping(create_tables=True)


def get_section_sort_prefs(request, sections):
    """ Read section sort preferences from cookies
    request: flask request object
    sections: list of sections on page
    Returns: dictionary of section sorting prefs, or None """
    sort_prefs = {}
    for section in sections:
        cookie = request.cookies.get(section + '_sort_pref')
        if not cookie:
            sort_prefs[section] = ''
        else:
            sort_prefs[section] = cookie
    return sort_prefs


def apply_new_section_expand(request, expand_prefs, req_path):
    """ Save new section expand preferences to cookie(s)
    request: flask request object
    expand_prefs: dictionary of section sorting prefs
    Returns: flask response with cookies set, or None """
    resp = None
    section = request.args.get('expand_toggle')
    if section in expand_prefs:
        expand_prefs[section] = expand_prefs[section] is not True
        resp = make_response(
            redirect(req_path
                     + '#' + urllib.parse.quote_plus('%s' % section), code=303))
        resp.set_cookie(section + '_expand', str(expand_prefs[section]),
                        expires=(datetime.datetime.now()
                                 + datetime.timedelta(days=9000)),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
    return resp


def get_section_expand_prefs(request, sections):
    """ Read section expand preferences from cookies
    request: flask request object
    sections: list of sections on page
    Returns: dictionary of section expand prefs, or None """
    expand_prefs = {}
    for section in sections:
        cookie = request.cookies.get(section + '_expand')
        if not cookie:
            expand_prefs[section] = False
        else:
            expand_prefs[section] = cookie.lower() == 'true'
    return expand_prefs


def apply_new_section_sort(request, sort_prefs, req_path):
    """ Save new section sorting preferences to cookie(s)
    request: flask request object
    sort_prefs: dictionary of section sorting prefs
    Returns: flask response with cookies set, or None """
    resp = None
    new_sort_col = request.args.get('sort_col')
    new_sort_section = request.args.get('sort_section')
    if new_sort_section in sort_prefs:
        direction = 'asc'
        if sort_prefs[new_sort_section]:
            try:
                old_col, old_direction = sort_prefs[new_sort_section].split(':')
            except ValueError:
                # Unable to unpack old sort preferences, so default to sorting
                # by new col and asc direction
                old_col, old_direction = (new_sort_col, 'asc')
            if new_sort_col == old_col:
                # Use opposite direction if section is already sorted by column
                if old_direction == 'asc':
                    direction = 'desc'
        # Save new sort preferences and redirect to clear querystring
        resp = make_response(
            redirect(req_path +
                     '#' + urllib.parse.quote_plus('%s' % new_sort_section),
                     code=303))
        resp.set_cookie(new_sort_section + '_sort_pref',
                        '%s:%s' % (new_sort_col, direction),
                        expires=(datetime.datetime.now()
                                 + datetime.timedelta(days=9000)),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
    return resp


def sort_section(section_dicts, sort_pref):
    """ Sort given section by column/direction
    section_dicts: list of dictionaries (representing rows)
    sort_pref: string in the form of '<column>:<direction>'
    Returns: sorted section_dicts or section_dicts if unable to sort """
    if not section_dicts:
        return []
    if sort_pref and ':' in sort_pref:
        try:
            col, direction = sort_pref.split(':')
        except ValueError:
            return section_dicts
        if col in section_dicts[0]:
            return sorted(section_dicts, key=lambda k: k[col],
                          reverse=direction == 'desc')
    return section_dicts


@app.template_filter('replace_for_js')
def replace_for_js(s):
    # escape/replace characters that trip up JS
    s = s.replace(' ', '_')
    s = s.replace('(', '')
    s = s.replace(')', '')
    s = s.replace('/', '')
    s = s.replace('\\', '')
    s = s.replace('&', '')
    s = s.replace('$', '')
    return s


@app.route("/")
@db_session
def main_page():
    sections = ['jobs']
    jobfilters = []
    jobfilter_str = ''
    # Apply any job filters if included in query string
    if 'jobfilter' in request.args:
        jobfilter_str = request.args.get('jobfilter')
        resp = make_response(redirect(url_for('main_page'), code=303))
        resp.set_cookie('jobfilter', jobfilter_str,
                        expires=datetime.datetime.now() +
                        datetime.timedelta(days=9000),
                        httponly=True, secure=secure_cookie,
                        samesite='Strict')
        return resp

    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs, url_for('main_page'))
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp
    # Get job filter preference
    jobfilter_str = request.cookies.get('jobfilter')
    if jobfilter_str:
        # Delimiter for terms is whitespace
        jobfilters = jobfilter_str.split()

    job_dicts = []
    jobs = orm.select(j for j in db.Job)[:]
    for job in jobs:
        builds = orm.select(b for b in db.Build
                            if b.job.id == job.id
                            ).sort_by(orm.desc(db.Build.build_date))[:1]
        if not builds:
            continue
        last_build_id = builds[0].build_id
        last_build_name = builds[0].name
        last_build_time = builds[0].build_date

        job_dicts.append({
            'name': job.name,
            'last_build_id': str(last_build_id),
            'last_build_name': last_build_name,
            'last_build_time': str(last_build_time),
        })

    # filter jobs based on user preference
    filtered_job_dicts = []
    if jobfilters:
        for jobfilter in jobfilters:
            if jobfilter is None:
                continue
            for job in job_dicts:
                if jobfilter in job['name'] and job not in filtered_job_dicts:
                    filtered_job_dicts.append(job)
        job_dicts = filtered_job_dicts

    job_dicts = sort_section(job_dicts, sort_prefs['jobs'])
    return render_template('main.html',
                           jobs=job_dicts,
                           top_links=[{"text": "Intel Mesa Performance CI",
                                       "href": url_for('main_page')}],
                           jobfilter=jobfilter_str, sort_prefs=sort_prefs)


@app.route("/<job_name>/builds")
@db_session
def builds_page(job_name):
    # Get sort preferences, save preferences if new ones are passed by
    # query string
    sections = ['builds']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs,
                                  url_for('builds_page', job_name=job_name))
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp
    job = orm.select(j for j in db.Job if j.name == job_name).first()
    su = request.args.get('su')
    del_build_id = request.args.get('delete')
    if del_build_id:
        print('deleting build with ID: ' + del_build_id)
        orm.delete(b for b in db.Build
                   if (b.build_id == del_build_id and b.job.id == job.id))
        orm.delete(r for r in db.Revision
                   if (r.build.build_id == del_build_id
                       and r.job.id == job.id))
        orm.delete(s for s in db.Score
                   if (s.build.build_id == del_build_id
                       and s.job.id == job.id))
        db.commit()
        return make_response(redirect(url_for('builds_page',
                                              job_name=job_name)))

    builds = orm.select([b.build_id, b.name, b.build_date, b.mesa_date, r.sha,
                         r.author, r.description]
                        for b in db.Build if b.job.id == job.id
                        for r in db.Revision if (r.build.build_id == b.build_id
                                                 and r.project == 'mesa'))[:]
    build_dicts = []
    for build in builds:
        build_dicts.append({
            'id': build[0],
            'name': build[1],
            'build_date': build[2],
            'mesa_date': build[3],
            'mesa_sha': build[4],
            'mesa_author': build[5],
            'mesa_desc': build[6],
        })
    # sort sections on page
    build_dicts = sort_section(build_dicts, sort_prefs['builds'])

    return render_template('builds.html', job=job_name, job_id=job.id,
                           builds=build_dicts,
                           top_links=[{"text": "Intel Mesa Performance CI",
                                       "href": url_for('main_page')},
                                      {"text": job_name,
                                       "href": url_for('builds_page',
                                                       job_name=job_name)}],
                           title='Builds for ' + job_name,
                           sort_prefs=sort_prefs, su=su)


@db_session
def get_closest_mesa_build(commit):
    repo_dir = os.environ.get("MESA_REPO_DIR", "")
    if not repo_dir:
        print("ERROR: No mesa repo dir set (export MESA_REPO_DIR to set)")
        return
    repo = git.Repo(os.path.expanduser(repo_dir))
    # revlist to get previous ~1000 commits ordered by date with most
    # recent first
    revlist = repo.git.rev_list([commit, '--max-count=1000',
                                 '--abbrev-commit']).split()

    # select all mesa master build_ids (using set for quick lookup)
    mesa_builds = orm.select(b.build_id for b in db.Build
                             if b.job.name == 'mesa_master')[:]
    mesa_builds = set(mesa_builds)
    # search for previous shas in mesa_builds, returning the first
    for sha in revlist:
        if sha in mesa_builds:
            return sha


@app.route("/<job_name>/history")
@db_session
def history_page(job_name):
    # Get sort preferences, save preferences if new ones are passed by
    # query string
    sections = ['history']
    sort_prefs = get_section_sort_prefs(request, sections)
    resp = apply_new_section_sort(request, sort_prefs,
                                  url_for('history_page', job_name=job_name))
    # If new preferences were given, reload/redirect to apply
    if resp:
        return resp
    job = orm.select(j for j in db.Job if j.name == job_name).first()
    benchmarks = orm.select(s.benchmark.name for s in db.Score if s.job.id == job.id)[:]
    cmp_job_id = request.args.get('job_id')
    cmp_build_id = request.args.get('build_id')
    base_build_id = request.args.get('base_build_id')
    base_build_date = None
    base_build_sha = None
    base_builds = orm.select(b for b in db.Build
                             if b.job == job).order_by(lambda d: orm.desc(d.mesa_date))

    if cmp_job_id and cmp_build_id:
        benchmark_dicts_cache_key = '_'.join(['benchmark_dicts', job.name,
                                              cmp_job_id, cmp_build_id])
        cmp_bm_dict_cache_key = '_'.join(['cmp_bm_dict', job.name,
                                          cmp_job_id, cmp_build_id])
        cmp_summary_cache_key = '_'.join(['cmp_summary', job.name,
                                          cmp_job_id, cmp_build_id])
    else:
        benchmark_dicts_cache_key = '_'.join(['benchmark_dicts', job.name])
        cmp_bm_dict_cache_key = None
        cmp_summary_cache_key = None

    benchmark_dicts = cache.get(benchmark_dicts_cache_key)
    cmp_bm_dict = cache.get(cmp_bm_dict_cache_key)
    cmp_summary = cache.get(cmp_summary_cache_key)
    if not benchmark_dicts:
        benchmark_dicts = []
        # key=build_id, val = dict of revisions for build
        rev_dict = {}
        for b in benchmarks:
            scores = orm.select(s for s in db.Score if s.benchmark.name == b
                                and s.job.id == job.id)[:]
            hardware = {}
            for score in scores:
                cur_hw = score.platform.gen_driver
                if cur_hw not in hardware:
                    hardware[cur_hw] = []

                # Note: Values below that are casted to int, rounded, etc are done
                # to reduce the page size sent to the client
                if score.build.build_id not in rev_dict:
                    revs = orm.select(rev for rev in db.Revision
                                      if rev.build.build_id == score.build.build_id)
                    rev_dict[score.build.build_id] = {}
                    for rev in [r for r in revs]:
                        rev_dict[score.build.build_id][rev.project] = {
                            'commit': rev.commit,
                            'author': rev.author,
                            'description': rev.description,
                            'sha': rev.sha,
                        }
                hardware[cur_hw].append({
                    'mesa_date': int(score.build.mesa_date.timestamp()),
                    'fps': round(score.score_avg, 2),
                    'std': round(score.std, 4),
                    'normal_std': round(score.normal_std, 4),
                    'sha': rev_dict[score.build.build_id]['mesa']['sha'],
                    'kern': score.kernel.replace('-amd64', ''),
                    'host': score.tester.hostname,
                    'raw': score.raw,
                })
                # sort graph points by mesa_date
                hardware[cur_hw].sort(key=lambda x: x['mesa_date'])

            benchmark_dicts.append({
                'name': b,
                'hardware': hardware
            })

    # Get benchmarks/data for comparison build if one is passed via query
    # string
    rev_dict = {}
    cmp_build_name = None
    cmp_job_name = None
    if cmp_job_id and cmp_build_id:
        cmp_summary = {}
        cmp_bm_dict = {}
        hardware = {}
        scores = orm.select(s for s in db.Score
                            if (s.build.build_id == cmp_build_id
                                and s.job.id == cmp_job_id))[:]
        cmp_mesa_sha = orm.select(r.sha for r in db.Revision
                                  if r.project == 'mesa'
                                  and r.build.build_id == cmp_build_id).first()
        cmp_build = orm.select(b for b in db.Build
                               if b.build_id == cmp_build_id
                               and b.job.id == cmp_job_id).first()
        cmp_build_name = cmp_build.name
        cmp_job_name = cmp_build.job.name
        if not base_build_id:
            # Preference is to use a branchpoint for comparison.
            # See if we have results for the branch point commit:
            if cmp_build.branchpoint:
                base_build_id = get_closest_mesa_build(cmp_build.branchpoint)
            else:
                base_build_id = get_closest_mesa_build(cmp_mesa_sha)
            base_build = orm.select(b for b in db.Build
                                    if b.job == job
                                    and b.build_id == base_build_id).first()
            if not base_build:
                # No stored branchpoint, or failed to find a close build.
                # Default to selecting latest mesa_master build
                print("unable to find close build for comparison to "
                      f"{cmp_build.branchpoint}")
                base_build = orm.select(b for b in db.Build
                                        if b.job == job
                                        ).order_by(lambda d: orm.desc(d.mesa_date)).first()
                base_build_id = base_build.build_id
        base_build_date = orm.select(b.mesa_date for b in db.Build
                                     if b.build_id == base_build_id).first()
        # webkit date parser doesn't support '-' for date delim. Everything (FF
        # and webkit) seem to support converting from a timestamp
        base_build_date = int(base_build_date.timestamp())
        base_build_sha = orm.select(r.sha for r in db.Revision
                                    if r.build.build_id == base_build_id
                                    and r.project.lower() == 'mesa').first()

        if not cmp_bm_dict or not cmp_summary:
            for score in scores:
                bm = score.benchmark

                if bm.name not in cmp_bm_dict:
                    cmp_bm_dict[bm.name] = {}
                cur_hw = score.platform.gen_driver
                if cur_hw not in cmp_bm_dict[bm.name]:
                    cmp_bm_dict[bm.name][cur_hw] = []

                # get t interval for showing bounds @ 95% confidence
                scale = score.std / math.sqrt(len(score.raw))
                t_lower = t_lower = None
                # last condition checks that the list is not all identical
                # items by converting to a set and checking the length
                if len(score.raw) > 1 and len(set(score.raw)) != 1:
                    print(f"{score.raw} {score.score_avg} {scale}")
                    t_lower, t_upper = stats.t.interval(0.95,
                                                        len(score.raw) - 1,
                                                        loc=score.score_avg,
                                                        scale=scale)

                bm_dict = {
                    'mesa_date': int(cmp_build.mesa_date.timestamp()),
                    'fps': round(score.score_avg, 2),
                    'std': round(score.std, 4),
                    'normal_std': round(score.normal_std, 4),
                    'sha': cmp_mesa_sha,
                    'kern': score.kernel.replace('-amd64', ''),
                    'host': score.tester.hostname.replace('otc-gfxperf-', ''),
                    'raw': score.raw,
                }
                if t_upper and t_lower:
                    bm_dict.update({
                        'fps_upper': round(t_upper, 2),
                        'fps_lower': round(t_lower, 2),
                    })
                cmp_bm_dict[bm.name][cur_hw].append(bm_dict)

                for mm_bm in benchmark_dicts:
                    add_to_summary = False
                    if mm_bm['name'] == bm.name:
                        mm_score = None
                        if score.platform.gen_driver not in mm_bm['hardware']:
                            # corresponding score on mesa_master might have
                            # been purged, so skip it
                            continue
                        for s in mm_bm['hardware'][score.platform.gen_driver]:
                            if s['sha'] == base_build_id:
                                mm_score = s
                                break
                        if not mm_score:
                            continue
                        try:
                            if t_lower and mm_score['fps'] < t_lower:
                                add_to_summary = True
                            elif t_upper and mm_score['fps'] > t_upper:
                                add_to_summary = True
                        except TypeError:
                            pass
                        delta = round(100 - (mm_score['fps'] / score.score_avg) *
                                      100, 2)
                        # ignore deltas within 1%
                        if abs(delta) < 1:
                            add_to_summary = False
                        if add_to_summary:
                            if bm.name not in cmp_summary:
                                cmp_summary[bm.name] = []
                            x1 = mm_score['fps']
                            x2 = score.score_avg
                            n1 = len(mm_score['raw'])
                            n2 = len(score.raw)
                            s1 = mm_score['std']
                            s2 = score.std
                            if n1 + n2 > 2:
                                pooled_std = math.sqrt((((n1-1)*s1**2) + ((n2-1)*s2**2))
                                                       / (n1 + n2 - 2))
                            else:
                                pooled_std = 0
                            delta = (x2 - x1) / x1 * 100
                            cmp_summary[bm.name] += [{
                                'benchmark': bm.name,
                                'hardware': score.platform.gen_driver,
                                'cmp_fps': round(score.score_avg, 2),
                                'orig_fps': round(mm_score['fps'], 2),
                                'cmp_std': round(score.std, 3),
                                'orig_std': round(mm_score['std'], 3),
                                'delta': round(delta, 2),
                                'pooled_std': round(pooled_std, 2),
                                'orig_sha': mm_score['sha'],
                            }]
    if benchmark_dicts:
        cache.set(benchmark_dicts_cache_key, benchmark_dicts, 1440)
    if cmp_bm_dict:
        cache.set(cmp_bm_dict_cache_key, cmp_bm_dict, 1440)
    if cmp_summary:
        cache.set(cmp_summary_cache_key, cmp_summary, 1440)

    return render_template('history.html', job=job_name,
                           benchmark_dicts=benchmark_dicts,
                           base_build_id=base_build_id,
                           base_build_date=base_build_date,
                           base_build_sha=base_build_sha,
                           base_builds=base_builds,
                           compare_build=cmp_bm_dict,
                           compare_build_name=cmp_build_name,
                           compare_job_name=cmp_job_name,
                           compare_job_id=cmp_job_id,
                           compare_build_id=cmp_build_id,
                           cmp_summary=cmp_summary,
                           top_links=[{"text": "Intel Mesa Performance CI",
                                       "href": url_for('main_page')},
                                      {"text": job_name,
                                       "href": url_for('builds_page',
                                                       job_name=job_name)}],
                           title='Builds for ' + job_name,
                           sort_prefs=sort_prefs)
