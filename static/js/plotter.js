function get_hw(dataset) {
    var hardwares = [];
    for (var hardware in dataset) {
        var lin_hardware = hardware.replace("win","");
        if (! hardwares.includes(lin_hardware)) {
            hardwares.push(lin_hardware);
        }
    }
    return hardwares;
}

function get_color(hw, alpha=1.0){
    // Generate a rgba() string using the given alpha for the given hardware
    var colors = {
        'gen9_iris':  "rgba(35, 106, 185,",  // #236AB9
        'gen9_vk':    "rgba(221, 11, 75,",   // #dd0b4b
        'gen11_iris': "rgba(148, 64, 237,",  // #9440ed
        'gen11_vk':   "rgba(252, 203, 26,",  // #FCCB1A
        'gen12_iris': "rgba(77, 223, 77,",   // #4ddf4d
        'gen12_vk':   "rgba(252, 115, 7,",   // #FC7307
    }
    color = colors[hw] + alpha + ")";
    return color;
}


// https://www.htmlgoodies.com/beyond/css/working_w_tables_using_jquery.html
function makeTable(container, data) {
    var table = $("<table/>").addClass('CSSTableGenerator');
    $.each(data, function(rowIndex, r) {
        var row = $("<tr/>");
        $.each(r, function(colIndex, c) {
            row.append($("<t"+(rowIndex == 0 ?  "h" : "d")+"/>").text(c));
        });
        table.append(row);
    });
    return container.append(table);
}


function do_plot(placeholder_id, click_id, dataset, cmp_dataset, cmp_build_name, base_build_date_ts, base_build_sha) {
    var data = []
    var hardwares = get_hw(dataset);
    var len = hardwares.length;
    var ymax = 0;
    var line_opacity = 1;
    // add platform checkboxes
    var choiceContainer = $(placeholder_id + "_hw_choices");
    $.each(dataset, function(hardware, val) {
        choiceContainer.append("<br/>" +
            "<input style='display: inline-block;' type='checkbox' name='" + placeholder_id + '_' + hardware +
            "' checked='checked' id='" + placeholder_id + '_' + hardware + "_checkbox'></input>" +
            "<label style='display: inline-block;' for='" + placeholder_id + '_' + hardware + "_checkbox'>"
            + hardware + "<div style='display: inline-block; width: 20px; height: 5px; background: " +
            get_color(hardware) + "; margin:3px;' /></label>"
        );
    });
    choiceContainer.find("input").click(plotAccordingToChoices);

    var cmpResultsContainer = $(placeholder_id + "_cmp_results");

    function plotAccordingToChoices() {
        var data = [];
        var cmp_data = [];
        var time_start = new Date().getTime() / 1000;
        var time_end = 0;
        cmpResultsContainer.html('')
        var cmpResultTable = [['GPU', 'Driver', 'Mean FPS', 'Std. Dev.', 'Confidence', 'Iterations']];
        var cmpResultAnnotations = [];
        var markings = [];
        choiceContainer.find("input:checked").each(function () {
            var hardware = $(this).attr("name").replace(placeholder_id + '_', '');

            if (hardware && dataset[hardware]) {
                var data_points = {
                    errorbars: "y",
                    show: true,
                    yerr: {show:true, upperCap: "-", lowerCap: "-"},
                    lineWidth: 3,
                    fillColor: get_color(hardware),
                }

                var d1 = {
                    label: hardware,
                    data: [],
                    points: data_points,
                    // TODO: set opacity to something lower if comparing to a single build
                    color: get_color(hardware),
                };
                for (var build in dataset[hardware]){
                    var build_data = dataset[hardware][build];
                    var score = build_data["fps"];
                    var deviation = build_data["std"];
                    var point = [build_data["mesa_date"], score];

                    if (build_data["mesa_date"] > time_end)
                        time_end = build_data["mesa_date"]
                    if (build_data["mesa_date"] < time_start)
                        time_start = build_data["mesa_date"]

                    if (deviation > 0.05) {
                        point.push(deviation);
                    } else {
                        point.push(0);
                    }
                    d1.data.push(point);
                    if (score + deviation > ymax) {
                        ymax = score + deviation;
                    }
                }
                data.push(d1);

                /*
                * This section adds elements for cases where the graph is making
                * a comparison.
                */
                if (cmp_dataset && hardware in cmp_dataset){

                    // Create score confidence interval lines for comparison build
                    var upper_line = {
                        label: hardware,
                        data: [],
                        points: {
                            show: false,
                        },
                        lines: {
                            lineWidth: 0,
                            fillColor: get_color(hardware, 0.3),
                            fill: true,
                        },
                        color: get_color(hardware),
                        fillBetween: hardware + '_lower_line',
                    };
                    var lower_line = {
                        id: hardware + '_lower_line',
                        label: hardware,
                        data: [],
                        points: {
                            show: false,
                        },
                        lines: {
                            lineWidth: 0,
                        },
                        color: get_color(hardware),
                    };
                    var mean_line = {
                        label: hardware,
                        data: [],
                        points: {
                            show: false,
                        },
                        lines: {
                            lineWidth: 1,
                        },
                        color: get_color(hardware),
                    };

                    var point_upper_start = [time_start, cmp_dataset[hardware][0]['fps_upper']];
                    var point_upper_end = [time_end, cmp_dataset[hardware][0]['fps_upper']];
                    upper_line.data.push(point_upper_start);
                    upper_line.data.push(point_upper_end);
                    data.push(upper_line);

                    var point_lower_start = [time_start, cmp_dataset[hardware][0]['fps_lower']];
                    var point_lower_end = [time_end, cmp_dataset[hardware][0]['fps_lower']];
                    lower_line.data.push(point_lower_start);
                    lower_line.data.push(point_lower_end);
                    data.push(lower_line);

                    var point_mean_start = [time_start, cmp_dataset[hardware][0]['fps']];
                    var point_mean_end = [time_end, cmp_dataset[hardware][0]['fps']];
                    mean_line.data.push(point_mean_start);
                    mean_line.data.push(point_mean_end);
                    data.push(mean_line);

                    // add hw result boxes under graph
                    var confidence_interval = Math.round((cmp_dataset[hardware][0]['fps_upper'] - cmp_dataset[hardware][0]['fps']) * 100) / 100;
                    var hw_drv = hardware.split('_');
                    var mean_fps = cmp_dataset[hardware][0]['fps'];
                    if (confidence_interval)
                        mean_fps += " ±" + confidence_interval
                    cmpResultTable.push([hw_drv[0],
                                         hw_drv[1],
                                         mean_fps,
                                         cmp_dataset[hardware][0]['std'],
                                         '95%',
                                         cmp_dataset[hardware][0]['raw'].length])

                    cmpResultAnnotations.push([hardware, time_end, cmp_dataset[hardware][0]['fps']]);

                    /* add verical marker indicating where the 'base' build being compared is on the graph */
                    markings = [{color: '#dd0000', lineWidth: 3, xaxis: {from: base_build_date_ts, to: base_build_date_ts}}];
                }
            }
        });
        // sort annotations by fps value
        cmpResultAnnotations.sort(function(a, b){ return a[2] - b[2]; });

        makeTable(cmpResultsContainer, cmpResultTable);

        ymax = Math.round(ymax * 10.0 + 0.5) / 10.0;
        yaxes = [{
            min: 0.0,
            max: ymax,
            zoomRange: false,
            axisLabel: 'FPS (higher is better)',
        }];
        if (cmp_dataset){
            yaxes.push({
                position: 'right',
                axisLabel: "Results for " + cmp_build_name,
                show: true,
                showTickLabels: 'none',
                showTicks: false,
                gridLines: false,
                axisLabelPadding: 60,
            });
        }
        if (data.length > 0) {

            var plot = $.plot(placeholder_id, data, {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 1,
                        zero: true,
                    },
                    points: {
                        show:true,
                    },
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    //autoHightlight: true,
                    borderWidth: 0,
                    margin: {
                        bottom: 20,
                    },
                    markings: markings,
                },
                yaxes: yaxes,
                xaxis: {
                    mode: "time",
                    tickSize: [7, "day"],
                    show: true,
                    axisLabel: 'mesa_master commit date'
                },
                zoom: {
                    interactive: true
                },
                pan: {
                    interactive: true
                },
            });

            // Add annotations to lines representing comparison build
            plot.hooks.drawOverlay.push(function(plot, canvascontext){
                $(placeholder_id).find($(".annotation")).remove();
                var prev_top = 0;
                for (var idx in cmpResultAnnotations){
                    var hardware = cmpResultAnnotations[idx][0];
                    var time_end = cmpResultAnnotations[idx][1];
                    var fps = cmpResultAnnotations[idx][2];
                    var offset = plot.pointOffset({ x: time_end, y: fps});

                    // Add fps
                    top_text_offset = 0;
                    left_text_offset = 20;
                    if (prev_top && Math.abs(fps - prev_top) < 20)
                        top_text_offset = 15;
                    left_text = offset.left + left_text_offset;
                    top_text = offset.top - top_text_offset;
                    $(placeholder_id).append("<div class='annotation' style='position:absolute;left:" + left_text + "px;top:" + (top_text - 8) + "px;color:" + "#666" + ";'>" + fps + "</div>");
                    prev_top = fps - top_text_offset;

                    // Draw triangle arrow
                    var ctx = plot.getCanvas().getContext("2d");
                    ctx.beginPath();
                    ctx.moveTo(offset.left + 2, offset.top);
                    ctx.lineTo(offset.left + 10, offset.top + 5);
                    ctx.lineTo(offset.left + 10, offset.top - 5);
                    ctx.lineTo(offset.left + 2, offset.top);
                    ctx.fillStyle = get_color(hardware);
                    ctx.fill();

                    ctx.beginPath();
                    ctx.moveTo(left_text, top_text);
                    ctx.lineTo(left_text - 4, top_text);
                    ctx.lineTo(offset.left + 14, offset.top);
                    ctx.lineTo(offset.left + 10, offset.top);
                    ctx.lineWidth = "2";
                    ctx.strokeStyle = get_color(hardware);
                    ctx.stroke();
                }
                if (base_build_date_ts){
                    // Add annotation for showing the base_build sha/label
                    var o;
                    axes = plot.getAxes();
                    o = plot.pointOffset({ x: base_build_date_ts, y: axes.yaxis.max + (axes.yaxis.max * .05)});
                    $(placeholder_id).append('<div style="position:absolute;left:' + (o.left - 80) + 'px;top:' + o.top + 'px;color:#666;font-size:smaller">' + base_build_sha + '</div>');
                }
            });
        }
    }

    plotAccordingToChoices();

    // Resize graph if window size changes
    $(window).resize(function() {plotAccordingToChoices();});

    function showTooltip(x, y, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 5,
            border: '1px solid ',
            padding: '2px',
            'background-color': '#fff'
        }).appendTo("body").fadeIn(200);
    }
    var previousPoint = null;
    $(placeholder_id).bind("plothover", function (event, pos, item) {
        if (item) {
            if (previousPoint != item.seriesIndex) {
                previousPoint = item.seriesIndex;

                $("#tooltip").remove();
                //var x = item.datapoint[0].toFixed(0),
                //y = item.datapoint[1].toFixed(0);

                showTooltip(item.pageX, item.pageY,
                    '<div><table><tr><td/><td/></tr>' +
                    '<tr><td>Hardware: </td>' +
                    '<td>' + item.series.label + '</td></tr>' +
                    '<tr><td>Kernel: </td>' +
                    '<td>' + dataset[item.series.label][item.dataIndex]["kern"] + '</td></tr>' +
                    '<tr><td>Score: </td>' +
                    '<td>' + (Math.round(item.series.data[item.dataIndex][1] * 1000) / 1000) + '</td></tr></table></div>'
                );
            }
        }
        else {
            $("#tooltip").remove();
            previousPoint = null;
        }
    });

    /* This function will build some data below the graph, information about
     * the commit, standard deviation, the averaged score of the tests, and
     * will build buttons to build a newer and older sha.
     */

    $(placeholder_id).bind("plotclick", function (event, pos, item) {
        if (item) {
            // Only try to get a previous sha if we are not on the oldest sha already
            if (item.dataIndex !== 0) {
                var prev_sha = dataset[item.series.label][item.dataIndex - 1]["sha"];
            } else {
                var prev_sha = '';
            }

            // The current sha is always valid
            var curr_sha = dataset[item.series.label][item.dataIndex]["sha"];

            // Only try to get the next sha if we are not on the newest sha
            if (item.dataIndex !== (dataset[item.series.label].length - 1)) {
                var next_sha = dataset[item.series.label][item.dataIndex + 1]["sha"];
            } else {
                var next_sha = '';
            }

            // Build the table of data and the raw html for the buttons
            $(click_id).html(
                '<div><table><tr><td/><td/></tr>' +
                '<tr><td>Hardware</td>' +
                '<td>' + item.series.label + '</td></tr>' +
                '<tr><td>Hostname</td>' +
                '<td>' + dataset[item.series.label][item.dataIndex]["host"] + '</td>' +
                '<tr><td>Kernel</td>' +
                '<td>' + dataset[item.series.label][item.dataIndex]["kern"] + '</td>' +
                '<tr><td>Commit</td>' +
                '<td><a href="https://cgit.freedesktop.org/mesa/mesa/commit/?id=' + curr_sha + '">' + curr_sha  + '</a></td>' +
                '<tr><td>Score</td>' +
                '<td>' + dataset[item.series.label][item.dataIndex]["fps"] + '</td>' +
                '<tr><td>Std. Dev.</td>' +
                '<td>' + dataset[item.series.label][item.dataIndex]["std"] + '</td>' +
                '</tr></table></div>'
            );
            // redraw plot in case the new table changes the container size to
            // overflow with the graph container
            plotAccordingToChoices();
        }
    });
}
