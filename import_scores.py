#!/usr/bin/env python3
# Copyright (C) Intel Corp.  2019.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *    Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
import datetime
import glob
import inotify.adapters
import json
import numpy
import os
import shutil
import subprocess
import tempfile
import time
from pony.orm import db_session
from pony import orm
from models import db


class Tarball:
    def __init__(self, tarfile):
        self.tarfile = tarfile

    def __enter__(self):
        self.tmpdir = tempfile.mkdtemp()
        return self

    def __exit__(self, type, value, traceback):
        os.remove(self.tarfile)
        shutil.rmtree(self.tmpdir)

    def process(self):
        self.extract()
        build_info = self.get_build_info(self.tmpdir + '/build_info.json')
        if not build_info:
            raise RuntimeError("ERR: Unable to process build_info for tar "
                               "file: %s" % self.tarfile)
        return build_info, self.tmpdir

    def extract(self):
        try:
            subprocess.check_output(['tar', 'xf', self.tarfile,
                                     '-C', self.tmpdir])
        except subprocess.CalledProcessError:
            raise RuntimeError("WARN: Unable to extract tar file: %s"
                               % self.tarfile)

    def get_build_info(self, build_info_file):
        build_info = None
        try:
            with open(build_info_file, 'r') as f:
                build_info = json.load(f)
        except FileNotFoundError:
            print("WARN: build_info json not found: %s" % build_info_file)
        return build_info


class ScoresImporter:
    def __init__(self, watch_dir, sql_socket=None, sql_user='jenkins',
                 sql_host='localhost'):
        self.sql_pw = ''
        self.sql_user = sql_user
        self.sql_host = sql_host
        self.sql_socket = sql_socket
        self.watch_dir = watch_dir
        if "SQL_DATABASE_PW_FILE" in os.environ:
            sql_pw_file = os.environ["SQL_DATABASE_PW_FILE"]
            if os.path.exists(sql_pw_file):
                with open(sql_pw_file, 'r') as f:
                    self.sql_pw = f.read().rstrip()
        self.inot = inotify.adapters.Inotify()
        self.inot.add_watch(self.watch_dir)
        connected = False
        while not connected:
            try:
                if self.sql_pw:
                    db.bind('mysql', host=self.sql_host, user=self.sql_user,
                            password=self.sql_pw, database='mesa_perf_results')
                else:
                    db.bind('mysql', host=self.sql_host, user=self.sql_user,
                            database='mesa_perf_results')
            except orm.dbapiprovider.OperationalError:
                time.sleep(5)
                continue
            connected = True
        db.generate_mapping(create_tables=True)

    def poll(self):
        """ Blocks until a new tarball is found by inotify, then
        returns the path when one is found """
        for event in self.inot.event_gen(yield_nones=False):
            (_, type_names, path, filename) = event
            new_file = path + "/" + filename
            # Only proceed if the event was close after write
            if 'IN_CLOSE_WRITE' not in type_names:
                continue
            if not new_file or not os.path.exists(new_file):
                continue
            return new_file

    @db_session
    def consume(self, tarfile):
        """ Import data in given tarfile """
        with Tarball(tarfile) as tarball:
            try:
                self.build_info, self.resultdir = tarball.process()
            except RuntimeError:
                print("Skipping broken tar...")
                return

            job = orm.select(j for j in db.Job
                             if j.name == self.build_info['job']).limit(1)
            if not job:
                job = db.Job(name=self.build_info['job'])
                # commit to get build_id in sql
                db.commit()
            else:
                job = job[0]
            build_id = self.build_info['revisions']['mesa']['sha']
            # delete old build if exists
            if orm.select(b for b in db.Build
                          if (b.build_id == build_id
                              and b.job.id == job.id)).exists():
                print("INFO: build already exists in database, removing and "
                      "replacing...")
                orm.delete(b for b in db.Build
                           if (b.build_id == build_id and b.job.id == job.id))
                orm.delete(r for r in db.Revision
                           if (r.build.build_id == build_id and r.job.id == job.id))
                orm.delete(s for s in db.Score
                           if (s.build.build_id == build_id and s.job.id == job.id))
                db.commit()
            print("Importing build: %s" % (self.build_info["name"]))
            build = db.Build(build_id=build_id,
                             name=self.build_info['name'],
                             build_date=datetime.datetime.fromtimestamp(
                                 self.build_info['start_time']),
                             mesa_date=datetime.datetime.fromtimestamp(
                                 self.build_info['mesa_date']),
                             job=job)
            if self.build_info.get("branchpoint"):
                build.branchpoint = self.build_info.get("branchpoint")
            revs = self.build_info['revisions']
            for proj in revs:
                db.Revision(project=proj, commit=revs[proj]['commit'],
                            author=revs[proj]['author'],
                            description=revs[proj]['description'],
                            sha=revs[proj]['sha'],
                            build=build,
                            job=job)
            score_files = glob.glob(self.resultdir + "/**/*.json",
                                    recursive=True)
            for score_file in score_files:
                # sometimes the glob picks this up, so it should be ignored
                if 'build_info.json' in score_file:
                    continue
                with open(score_file, 'r') as f:
                    file = json.load(f)
                    scores = []
                    if file.get('scores'):
                        # remove any None scores from the list of scores by
                        # creating a new list so that the length is correct
                        scores = [s for s in file['scores'] if s]
                    if not scores:
                        print("ERROR: no scores found in the following file, "
                              "skipping: " + score_file)
                        continue
                    benchmark = orm.select(b for b in db.Benchmark
                                           if b.name == file['benchmark']).first()
                    if not benchmark:
                        benchmark = db.Benchmark(name=file['benchmark'],
                                                 project=file['project'])
                        # commit to get id in sql
                        db.commit()
                    gen_driver = '_'.join([file['gen'], file['driver']])
                    platform = orm.select(p for p in db.Platform
                                          if p.gen_driver == gen_driver).first()
                    if not platform:
                        platform = db.Platform(gen_driver=gen_driver)
                        # commit to get id in sql
                        db.commit()
                    tester = orm.select(t for t in db.Tester
                                        if t.hostname == file['hostname']).first()
                    if not tester:
                        tester = db.Tester(hostname=file['hostname'])
                        # commit to get id in sql
                        db.commit()
                    score_avg = numpy.mean(scores, dtype=numpy.float16)
                    score_std = numpy.std(scores, dtype=numpy.float16)
                    sys_config = file.get('sys_config')
                    db.Score(score_avg=score_avg,
                             normal_std=score_std / score_avg,
                             std=score_std,
                             kernel=sys_config.get('kernel_version'),
                             benchmark=benchmark,
                             job=job,
                             build=build,
                             tester=tester,
                             platform=platform,
                             sys_config_hash=file.get('sys_config_hash'),
                             sys_config=file.get('sys_config'),
                             raw=scores)

                    db.commit()

        print("Done processing tarball: %s" % tarfile)


def main():
    results_path = '/tmp/mesa_perf_results'
    if not os.path.exists(results_path):
        os.mkdir(results_path)

    sql_host = 'localhost'
    if "SQL_DATABASE_HOST" in os.environ:
        sql_host = os.environ["SQL_DATABASE_HOST"]

    sql_user = 'jenkins'
    if "SQL_DATABASE_USER" in os.environ:
        sql_user = os.environ["SQL_DATABASE_USER"]

    sql_socket = None
    if "SQL_DATABASE_SOCK" in os.environ:
        sql_socket = os.environ["SQL_DATABASE_SOCK"]
    results_importer = ScoresImporter(results_path, sql_host=sql_host,
                                      sql_user=sql_user,
                                      sql_socket=sql_socket)
    # consume any existing tar files
    for tarfile in glob.glob(results_path + "/*.xz"):
        results_importer.consume(tarfile)
    while True:
        tarfile = results_importer.poll()
        results_importer.consume(tarfile)


if __name__ == "__main__":
    main()
