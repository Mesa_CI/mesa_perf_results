import datetime
from pony import orm

db = orm.Database()


class Job(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    name = orm.Required(str, unique=True)
    builds = orm.Set('Build')
    scores = orm.Set('Score')
    Revision = orm.Set('Revision')


class Benchmark(db.Entity):
    name = orm.PrimaryKey(str)
    project = orm.Required(str)
    scores = orm.Set('Score')
    blocklist = orm.Set('Blocklist')


class Platform(db.Entity):
    # gen_driver is, e.g. gen9_iris
    gen_driver = orm.PrimaryKey(str)
    blocklist = orm.Set('Blocklist')
    scores = orm.Set('Score')


class Tester(db.Entity):
    hostname = orm.PrimaryKey(str)
    scores = orm.Set('Score')


class Build(db.Entity):
    # build_id is mesa sha, e.g. 8145492f4aa
    build_id = orm.Required(str)
    name = orm.Required(str)
    build_date = orm.Required(datetime.datetime)
    mesa_date = orm.Required(datetime.datetime)
    job = orm.Required(Job)
    # used for builds that are not mesa_master,
    # either the branchpoint or the closest thing to it:
    branchpoint = orm.Optional(str)
    revisions = orm.Set('Revision')
    scores = orm.Set('Score')
    orm.composite_index(build_id, job)


class Score(db.Entity):
    score_avg = orm.Required(float)
    std = orm.Required(float)
    # std, normalized
    normal_std = orm.Required(float)
    kernel = orm.Required(str)
    benchmark = orm.Required(Benchmark)
    job = orm.Required(Job)
    build = orm.Required(Build)
    tester = orm.Required(Tester)
    platform = orm.Required(Platform)
    # list of raw scores
    raw = orm.Required(orm.Json)
    sys_config = orm.Optional(orm.Json)
    sys_config_hash = orm.Optional(str)
    orm.composite_index(build, job)


class Revision(db.Entity):
    id = orm.PrimaryKey(int, auto=True)
    project = orm.Required(str)
    commit = orm.Required(str)
    author = orm.Required(str)
    description = orm.Required(str)
    sha = orm.Required(str)
    build = orm.Required(Build)
    job = orm.Required(Job)
    orm.composite_index(build, job)


class Blocklist(db.Entity):
    """
    Table for project/platform combos that have failed in CI and should not be
    re-run by automation
    """
    # ID is a sha1 hash of build_id + benchmark.project + platform.gen_driver
    id = orm.PrimaryKey(str)
    build_id = orm.Required(str)
    platform = orm.Required(Platform)
    # optional so that entire platforms can be added
    benchmark = orm.Optional(Benchmark)
